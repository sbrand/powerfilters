%-------------------------------------------------------------------------
% Optical model for the calculation of the Reflectance and Transmittance 
% of a multi-layer dielectric stack 

% Based on the paper:
% Abel�s, F. (1950). Recherches sur la propagation des ondes 
% electromagnetiques sinusoidales dans les milieux stratifies: application 
% aux couches minces Abel�s. Ann. Phys, 5, 596-640.

% Assumptions: Multilayer structure composed of optically isotropic and
% homogeneous layers with plane and parallel faces. The substrate is
% assumed to be semi-infinite.

% PowerFilters is provided as open source software, and is released under 
% the GNU General Public License (http://www.gnu.org/copyleft/gpl.html)

% Project Started 17 June 2011
% Last update 25-8-2011

% Created by: Aaron Jerling
% Created with: Matlab v. 2011(a)

% Notes: This code has only been partially validated - any suggestions,
% improvements or extensions would be warmly received.
%-------------------------------------------------------------------------
clear all;
close all;
clc;

design_wavelength = 1545e-9;
% Most coatings only require two types of material. To add another material
% simply add another variable here your_medium_n and call it within the
% layer-construction loops
low_n =  1.44875;
high_n = 2.21302;

light_sees_layer_1_last = 0; % 1 = True, 0 = False

% Incident medium


n_incident = high_n; % Refractive index
incident_angle = 0; % in radians!

% Substrate medium
n_substrate = high_n; %Refractive index
sub_thickness = 3e-3; % in meters (only applies if backside is 
% considered otherwise the substrate is considered to be semi-infinite )

% is the light p or s polarised?
%1 = p-polarised, 2 = s-polarised, 3 = un-polarised(p-pol)
polarisation = 1;

% Wavelength range over which to model
start_wav = 1540; %in nm
end_wav = 1550; %in nm
wav_inc = 0.01; %in nm

%-------------------------------------------------------------------------
% To begin we put together the dielectric stack in order from the incident
% medium to the exit / outside medium - usually the substrate. so layer 1 =
% outermost layer (next to incident medium)
%-------------------------------------------------------------------------

%=========================================================================%
%================= I N C I D E N T == M E D I U M ========================%
%=========================================================================%
%=============================== LAYER 1==================================%
%=============================== LAYER 2==================================%
%================================ o o o ==================================%
%=============================== LAYER N - 1==============================%
%=============================== LAYER N==================================%
%=========================================================================%
%========================= S U B S T R A T E =============================%
%=========================================================================%
%================= B A C K S I D E == M E D I U M ========================%
%=========================================================================%

layer_number = 0;
% same layers loop
number_same_layers = 6;
for layer = 1:1:number_same_layers,
    %low_layer SiO2
    layer_number = layer_number + 1;
    layer_ref_ind(layer_number) = low_n; %Refractive index
    layer_trans_angle(layer_number) = asin ( (n_incident / layer_ref_ind(layer_number) ) * sin(incident_angle) );
    optical_thickness = 0.25 * design_wavelength;
    layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters
    %high_layer TiO2
    layer_number = layer_number + 1;
    layer_ref_ind(layer_number) = high_n; %Refractive index
    layer_trans_angle(layer_number) = asin ( ( layer_ref_ind(layer_number-1) / layer_ref_ind(layer_number) ) * sin(layer_trans_angle(layer_number-1)) );
    optical_thickness = 0.25 * design_wavelength;
    layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters
end

%spacer_layer
number_same_layers = 14;
for layer = 1:1:number_same_layers,
    %low_layer SiO2
    layer_number = layer_number + 1;
    layer_trans_angle(layer_number) = 0;
    layer_ref_ind(layer_number) = low_n; %Refractive index
    optical_thickness = 0.25 * design_wavelength;
    layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters
end

% same layers loop
number_same_layers = 5;
for layer = 1:1:number_same_layers,
    layer_number = layer_number + 1;
    layer_ref_ind(layer_number) = high_n; %Refractive index
    layer_trans_angle(layer_number) = asin ( ( layer_ref_ind(layer_number-1) / layer_ref_ind(layer_number) ) * sin(layer_trans_angle(layer_number-1)) );
    optical_thickness = 0.25 * design_wavelength;
    layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters

    %low_layer SiO2
    layer_number = layer_number + 1;
    layer_ref_ind(layer_number) = low_n; %Refractive index
    layer_trans_angle(layer_number) = asin ( ( layer_ref_ind(layer_number-1) / layer_ref_ind(layer_number) ) * sin(layer_trans_angle(layer_number-1)) );
    optical_thickness = 0.25 * design_wavelength;
    layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters
end

%high_layer TiO2
layer_number = layer_number + 1;
layer_ref_ind(layer_number) = high_n; %Refractive index
layer_trans_angle(layer_number) = asin ( ( layer_ref_ind(layer_number-1) / layer_ref_ind(layer_number) ) * sin(layer_trans_angle(layer_number-1)) );
optical_thickness = 0.25 * design_wavelength;
layer_thickness(layer_number) = real(optical_thickness / layer_ref_ind(layer_number)); %in meters

%-------------------------------------------------------------------------
% Layer construction finished now set the constants and start the
% mathematics
%-------------------------------------------------------------------------

e0 = 8.8542e-12; % permittivity of free space
u0 = pi*4e-7; %permeability of free space
nu0 = sqrt(e0/u0);

%-------------------------------------------------------------------------
% this first loop enables the calculation of reflectance 
% over multiple wavelengths
%-------------------------------------------------------------------------
j = 0;
for index = start_wav:wav_inc:end_wav,
% the j index is used for recording the reflectance / transmittance at
% each particular wavelength
    j = j + 1;
    wavelength(j) = index * 1e-9;
    M = zeros(2,2,layer_number);
% the following loop cycles through each layer calculating its
% characteristic matrix
    for index2 = 1:1:(layer_number),
        n = layer_ref_ind(index2);
        d = layer_thickness(index2);
        layer_angle = layer_trans_angle(index2); 
        phase_shift =( (2*pi)  / wavelength(j) ) * (2 * n * d * cos(layer_angle)) * 0.5;
        
        pseudo_n_s = (nu0*n) * cos(layer_angle); 
        pseudo_n_p = nu0*n / cos(layer_angle); 
       
        if polarisation == 1, %p-polarisation
            pseudo_n = pseudo_n_p;
        elseif polarisation == 2,%s-polarisation
            pseudo_n = pseudo_n_s;
        else % unpolarised or anthing else.
            pseudo_n = pseudo_n_s;
        end

% the next lines calculate the characteristic matrix of the thin
% film and store them in the M array
        mat11 = cos(phase_shift);
        mat12 = (1/pseudo_n) * 1i *sin(phase_shift);
        mat21 = pseudo_n * 1i * sin(phase_shift);
        mat22 = cos(phase_shift);
        M(:,:,index2) = [mat11 mat12;mat21 mat22];
    end

    the_matrix = [1 0;0 1];
    for index3 = 1:1:(layer_number),
        if (light_sees_layer_1_last > 0),
            the_layer = layer_number - (index3 - 1);
        else
            the_layer = index3; 
        end
        the_matrix = the_matrix * M(:,:,the_layer);
    end
    
% The following four lines make up the characteristic matrix of the
% filter in the forward direction
    m11 = the_matrix(1);
    m21 = the_matrix(2);
    m12 = the_matrix(3);
    m22 = the_matrix(4);
       
% Calculate the pseudo- indices of the incident and exit media
     if polarisation == 1, %p-polarisation
        Y0 = nu0 * n_incident / cos(incident_angle);
        Ys = nu0 * n_substrate / cos(layer_angle);
     elseif polarisation == 2,%s-polarisation
        Y0 = nu0 * n_incident * cos(incident_angle);
        Ys = nu0 * n_substrate * cos(layer_angle);
     else % unpolarised or anthing else.
        Y0 = nu0 * n_incident * cos(incident_angle);
        Ys = nu0 * n_substrate * cos(layer_angle);
     end
     
% Calculate the reflectance due to the filter
    reflect_num = (Y0*m11) + (Y0*Ys*m12) - m21 - (Ys*m22);
    reflect_den = (Y0*m11) + (Y0*Ys*m12) + m21 + (Ys*m22);
    reflect = reflect_num / reflect_den;
    reflectance(j) = reflect * conj(reflect);
    
% Calculate the transmittance due to the filter
    transmit_num = 2 * Y0;
    transmit_den = (Y0*m11) + (Y0*Ys*m12) + m21 + (Ys*m22);
    transmit = transmit_num / transmit_den;
    transmittance(j)=(real(n_substrate) / real(n_incident))* transmit * conj(transmit); 
end

figure 
plot(wavelength, reflectance, wavelength, transmittance)



