# What is PowerFilters?

PowerFilters is an optical model for the calculation of the Reflectance and Transmittance of a multi-layer dielectric stack.

# About the project

This project was a small part of a larger PhD project undertaken by Aaron Jerling at the Department of Medical Physics and Bioengineering, University College London, UK.

# Theoretical basis of the model

This model is based on the paper: Abelès, F. (1950). Recherches sur la propagation des ondes electromagnetiques sinusoidales dans les milieux stratifies: application aux couches minces Abelés. Ann. Phys, 5, 596-640.

# Assumptions

1. Multilayer structure composed of optically isotropic and homogeneous layers with plane and parallel faces.
2. The substrate is assumed to be semi-infinite.

# Licencing

PowerFilters is provided as open source software, and is released under the GNU General Public License (http://www.gnu.org/copyleft/gpl.html)

# Authors

Created by: Aaron Jerling

Created with: Matlab v. 2011(a)

# Notes: 

This code has only been partially validated - any suggestions, improvements or extensions would be warmly received.

# Release history:

## version 0.1

released 17 June 2011

## version 0.2

released 25 August 2011

## version 0.3

released 10 April 2012

#### changes

1. Fixed a small bug which considered the layer order incorrectly.
2. Included 2 scripts which serve as validation and examples on how to use the code.
